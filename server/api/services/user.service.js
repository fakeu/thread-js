import userRepository from '../../data/repositories/user.repository';

export const getUserById = async (userId) => {
    const { id, username, email, imageId, image, userStatus } = await userRepository.getUserById(
        userId
    );
    return { id, username, email, imageId, image, userStatus };
};

export const update = ({ id, ...data }) => userRepository.updateById(id, data);
