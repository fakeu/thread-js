export default {
    up: (queryInterface, Sequelize) => queryInterface.sequelize.transaction(transaction => Promise.all([
        queryInterface.addColumn('users', 'userStatus', {
            type: Sequelize.STRING,
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
        })
    ])),

    down: queryInterface => queryInterface.sequelize.transaction(transaction => Promise.all([queryInterface.removeColumn('users', 'userStatus', { transaction })]))
};
