import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getUserImgLink } from "src/helpers/imageHelper";
import { Grid, Image, Input, Button, Icon } from "semantic-ui-react";
import { updateUser } from "./actions";

import * as imageService from "src/services/imageService";

class Profile extends React.Component {
  constructor() {
    super();

    this.state = {
      isUploading: false,
      userStatusText: "",
      usernameText: "",
      usermailText: ""
    };
    this.setStatus = this.setStatus.bind(this);
    this.updateUserInfo = this.updateUserInfo.bind(this);
    this.changeUserStateText = this.changeUserStateText.bind(this);
  }

  componentDidMount() {
    this.setState({
      userStatusText: this.props.user.userStatus,
      usernameText: this.props.user.username,
      usermailText: this.props.user.email
    });
  }

  handleUploadFile = async ({ target }) => {
    this.setState({ isUploading: true });
    try {
      const { id: imageId, link: imageLink } = await imageService.uploadImage(
        target.files[0]
      );
      this.setState({ imageId, imageLink, isUploading: false });

      this.props.updateUser({ ...this.props.user, imageId, image: imageLink });
    } catch {
      // TODO: show error
      this.setState({ isUploading: false });
    }
  };

  changeUserStateText(key, text) {
    this.setState({
      [key]: text.target.value
    });
  }

  setStatus() {
    this.props.updateUser({
      ...this.props.user,
      userStatus: this.state.userStatusText
    });
  }

  updateUserInfo() {
    this.props.updateUser({
      ...this.props.user
    });
  }

  render() {
    const { isUploading } = this.state;

    const { user } = this.props;

    console.log(user);

    return (
      <Grid container textAlign="center" style={{ paddingTop: 30 }}>
        <Grid.Column>
          <br />
          <br />
          <Input
            icon=""
            iconPosition="left"
            placeholder="Email"
            type="text"
            value={this.state.userStatusText}
            onChange={text => this.changeUserStateText("userStatusText", text)}
          />
          <button onClick={() => this.setStatus()}>Set Status</button>
          <Image
            centered
            src={getUserImgLink(user.image)}
            size="medium"
            circular
          />
          <br />
          <Input
            icon="user"
            iconPosition="left"
            placeholder="Username"
            type="text"
            value={this.state.usernameText}
            onChange={text => this.changeUserStateText("usernameText", text)}
          />
          <br />
          <br />
          <Input
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            value={this.state.usermailText}
            onChange={text => this.changeUserStateText("usermailText", text)}
          />
          <br />
          <br />
          <Button
            color="teal"
            icon
            labelPosition="left"
            as="label"
            loading={isUploading}
            disabled={isUploading}
          >
            <Icon name="image" />
            Update avatar
            <input
              name="image"
              type="file"
              onChange={this.handleUploadFile}
              hidden
            />
          </Button>
        </Grid.Column>
      </Grid>
    );
  }
}

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any)
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});
const actions = { updateUser };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
