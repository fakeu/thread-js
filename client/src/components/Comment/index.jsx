import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon } from 'semantic-ui-react';

import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = (props) => {
    const {
        comment: { body, createdAt, user, id }
    } = props;
    const date = moment(createdAt).fromNow();
    const likePost = (cid, value) => {
        console.log(cid, value);
    };

    return (
        <CommentUI className={styles.comment}>
            <CommentUI.Avatar src={getUserImgLink(user.image)} />
            <CommentUI.Content>
                <CommentUI.Author as="a">{user.username}</CommentUI.Author>
                <CommentUI.Metadata>{date}</CommentUI.Metadata>
                <CommentUI.Text>{body}</CommentUI.Text>
            </CommentUI.Content>
            <Label basic size="small" as="a" onClick={() => likePost(id, true)}>
                <Icon name="thumbs up" />
                {1}
            </Label>
            <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => likePost(id, false)}
            >
                <Icon name="thumbs down" />
                {2}
            </Label>
        </CommentUI>
    );
};

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Comment;
