import callWebApi from 'src/helpers/webApiHelper';

export const updateUser = async (request) => {
    try {
        const response = await callWebApi({
            endpoint: '/api/auth/user',
            type: 'PUT',
            request
        });
        return response.json();
    } catch (e) {
        return null;
    }
};
